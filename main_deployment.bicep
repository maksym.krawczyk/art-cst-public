
targetScope = 'subscription'

param ch string = 'switzerlandnorth' 
resource resourceGroup1 'Microsoft.Resources/resourceGroups@2021-04-01' = {
  name: 'Atomic-Test-RG'
  location: ch
}

resource resourceGroup2 'Microsoft.Resources/resourceGroups@2021-04-01' = {
  name: 'Atomic-Test-Testground-RG'
  location: ch
}

module resourceGroupModule 'resourceGroup.bicep' = {
  scope: resourceGroup1
  name: 'resourceGroupModule'
}

