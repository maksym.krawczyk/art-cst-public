<div align="center">
  <center><h1>Atomic Red Team - Continuous Security Testing</h1></center>
</div>
This repository contains template files needed to deploy infrastructure on clinet's Azure tenant during enrollment into ART-CST project.<br>
Template will create the following resources:
<ul style="padding-left:10px">
  <li>Atomic-Test-Testground-RG - Empty RG  that will host testing VMs</li>
  <li>Atomic-Test-RG - This RG will fascilitate the testing and contain test logs. </li>
  <ul style="padding-left:10px">
    <li>    artcststorage - storageAccount</li>
    <li>    DCE - Data Collection Endpoint </li>
    <li>    DCR - Data Collection Rule</li>
    <li>    ART-CST-logs - log analytics workspace</li>
  </ul>
  
</ul>
Press the below button to deploy to your tenant:

[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.switch.ch%2Fmaksym.krawczyk%2Fart-cst-public%2F-%2Fraw%2Fmain%2Fazuredeploy.json)
