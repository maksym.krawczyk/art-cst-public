targetScope = 'resourceGroup'

param ch string = 'switzerlandnorth' 

resource storageAccount 'Microsoft.Storage/storageAccounts@2022-05-01' = {
  name: 'artcststorage'
  location: ch
  sku: {
    name: 'Standard_LRS'
  }
  kind: 'StorageV2'
}

resource DCE 'Microsoft.Insights/dataCollectionEndpoints@2022-06-01' = {
  name: 'DCE'
  location: ch
  properties: {
    configurationAccess: {}
    logsIngestion: {}
    networkAcls: {
      publicNetworkAccess: 'Enabled'
    }
  }
}

resource loganalytics 'Microsoft.OperationalInsights/workspaces@2022-10-01' = {
  name: 'ART-CST-logs'
  location: ch
  properties: {
    sku: {
      name: 'pergb2018'
    }
    retentionInDays: 30
    features: {
      enableLogAccessUsingOnlyResourcePermissions: true
    }
    workspaceCapping: {
      dailyQuotaGb: -1
    }
    publicNetworkAccessForIngestion: 'Enabled'
    publicNetworkAccessForQuery: 'Enabled'
  }
}

resource CustomLogs 'Microsoft.OperationalInsights/workspaces/linkedstorageaccounts@2020-08-01' = {
  parent: loganalytics
  name: 'CustomLogs'
  location: ch
  properties: {
    storageAccountIds: [
      storageAccount.id
    ]
  }
}

resource AtomicTestResults_CL 'Microsoft.OperationalInsights/workspaces/tables@2021-12-01-preview' = {
  parent: loganalytics
  name: 'AtomicTestResults_CL'
  properties: {
    totalRetentionInDays: 90
    plan: 'Analytics'
    schema: {
      name: 'AtomicTestResults_CL'
      columns: [
        {
          name: 'TimeGenerated'
          type: 'datetime'
        }
        {
          name: 'DeploymentLog'
          type: 'string'
        }
        {
          name: 'Tactic'
          type: 'string'
        }
        {
          name: 'Detected'
          type: 'boolean'
        }
      ]
    }
    retentionInDays: 30
  }
}


resource DCR 'Microsoft.Insights/dataCollectionRules@2022-06-01' = {
  name: 'DCR'
  location: ch 
  properties: {
    dataCollectionEndpointId: DCE.id
    streamDeclarations: {
      'Custom-AtomicTestResults_CL': {
        columns: [
          {
            name: 'TimeGenerated'
            type: 'datetime'
          }
          {
            name: 'DeploymentLog'
            type: 'string'
          }
          {
            name: 'Tactic'
            type: 'string'
          }
          {
            name: 'Detected'
            type: 'boolean'
          }
        ]
      }
    }
    destinations: {
      logAnalytics: [
        {
          workspaceResourceId: loganalytics.id
          name: 'ART-CST_LogAnalytics'
        }
      ]
    }
    dataFlows: [
      {
        streams: [
          'Custom-AtomicTestResults_CL'
        ]
        destinations: [
          'ART-CST_LogAnalytics'
        ]
        transformKql: 'source'
        outputStream: 'Custom-AtomicTestResults_CL'
      }
    ]
  }
  dependsOn: [
    CustomLogs
  ]
}

